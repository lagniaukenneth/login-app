import re

from user import User

users = [
    User("Kenneth", 123, "hotmail"),
    User("Fien", 123, "gmail"),
    User("Kathleen", 123, "outlook"),
]

print("Welcome user, please enter a choice")
print("•1. Sign in")
print("•2. Create new user")
print("•3. Exit")

# TODO : while loop voor herhaling

input_choice = input("Please enter your choice (1-3)")
if input_choice == "1":
    input_username = input("Enter your username: ")
    for user in users:
        correct_username = (input_username == user.username)
        if correct_username:
            print(f"Welcome {input_username}")
            if int(input(f"Hey {input_username}, please enter your password: ")) == user.password:
                print("You have succesfully logged in! ")
                print("*********************************")
                print("•1. Change password")
                print("•2. Change e-mail address")
                print("•3. Sign out")
                select_user_input = input("Please enter your value (1-3): ")
                if select_user_input == "1":
                    user.change_pwd(input("Enter your new password please: "))
                    print(f"Your password has been changed to: {user.password} !")
                elif select_user_input == "2":
                    user.change_email(input("Enter your new e-mail address (user@gmail.com) for example: "))
                    print(f"Your e-mail has been changed to: {user.email} !")
                elif select_user_input == "3":
                    quit()


elif input_choice == "2":
    print("Here you can create a new account! ")
    create_username = input("Please enter an username: ")

    create_email = input("Please enter an email-address: ")

    create_password = input("Please enter a password: ")
    created_user = User(
        create_username,
        create_password,
        create_email
    )
    users.append(created_user)
    print("New account has been added! ")
    # print(f"Welcome, {created_username.username}, ")

elif input_choice == "3":
    print("See you next time! ")
    exit()


