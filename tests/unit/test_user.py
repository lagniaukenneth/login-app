from unittest import TestCase
from user import User


class TestUser(TestCase):
    def test_create_user(self):
        dummy_user = User(
            "test",
            "test",
            "test"
        )
        self.assertEqual("test", dummy_user.username)
        self.assertEqual("test", dummy_user.password)
        self.assertEqual("test", dummy_user.email)

    def test_change_pwd_to_short_throws_exception(self):
        pwd = "A123"
        dummy_user = User(
            "test username",
            "test password",
            "user@gmail.com"
        )
        # ACT
        # dummy_user.change_pwd(pwd)

        # Assert
        self.assertRaises(Exception, dummy_user.change_pwd, pwd)

    def test_change_pwd_does_not_contain_nums_exception(self):
        pwd = "blablabla"
        dummy_user = User(
            "testerusername",
            "testpwd",
            "testemail"
        )

        self.assertRaises(Exception, dummy_user.change_pwd, pwd)

    def test_change_pwd_does_not_contain_letters_throws_exception(self):
        pwd = "123"
        dummy_user = User(
            "testusername",
            "testpwd",
            "testemail"
        )

        self.assertRaises(Exception, dummy_user.change_pwd, pwd)

    def test_change_pwd_valid_bv_lower(self):
        pwd = "a1234567"
        dummy_user = User(
            "testusername",
            "testpwd",
            "testemail"
        )

        dummy_user.change_pwd(pwd)
        self.assertEqual(pwd, dummy_user.password)

    def test_change_email_no_at_symbol_invalid(self):
        email = "user.com"
        dummy_user = User(
            "testusername",
            "testpwd",
            "user.com"
        )

        self.assertEqual(email, dummy_user.email)

    def test_change_email_no_period_invalid(self):
        email = "user@hotmailcom"
        dummy_user = User(
            "testusername",
            "testpwd",
            "user@hotmailcom"
        )

        self.assertEqual(email, dummy_user.email)

    def test_change_email_no_domain_invalid(self):
        email = "user@.com"
        dummy_user = User(
            "testusername",
            "testpwd",
            "user@.com"
        )

        self.assertEqual(email, dummy_user.email)

    def test_change_email_no_extension_invalid(self):
        email = "user@hotmail"
        dummy_user = User(
            "testusername",
            "testpwd",
            "user@hotmail"
        )

        self.assertEqual(email, dummy_user.email)

    def test_change_email_valid(self):
        # email = "user@gmail.com"
        dummy_user = User(
            "testusername",
            "testpwd",
            "user@gmail.com"
        )

        self.assertEqual("user@gmail.com", dummy_user.email)
