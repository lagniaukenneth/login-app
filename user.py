import re


# FORMULIER, bekijk dit als een formulier waar gebruik van kan gemaakt worden.
class User:
    def __init__(self, username, password, email):
        self.username = username
        self.password = password
        self.email = email

    def change_pwd(self, new_pwd):
        is_long_enough = (len(new_pwd) >= 8)
        contains_letters = re.search("[a-zA-Z]", new_pwd)
        contains_nums = re.search("[0-9]", new_pwd)

        pwd_is_strong = is_long_enough and contains_nums and contains_letters

        if pwd_is_strong:
            self.password = new_pwd
        else:
            raise Exception("Password is too weak")

    def change_email(self, new_email):
        valid_email = re.match('[a-z0-9._-]+@[a-z0-9._-]+\.[a-z._-]', new_email)
        if valid_email:
            self.email = new_email
        else:
            raise Exception("Email needs for example 'user@gmail.com'")
